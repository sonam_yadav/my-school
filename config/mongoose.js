/**
 * Created by comtech on 16/4/16.
 */
var config = require('./config'),
    mongoose = require('mongoose');


module.exports = function() {
    console.log('config====',config)
    var db = mongoose.connect(config.db);
    require('../app/models/user.server.model');
    require('../app/models/students');
    require('../app/models/subjects');
    require('../app/models/teachers');


    return db;
};
