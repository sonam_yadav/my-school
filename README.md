# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
*Live demo can be found on:
[http://myapp.sonamyadav.com/#/](http://myapp.sonamyadav.com/#/)

* Quick summary
A prototype for storing details of students,teachers and subjects in a school.
There are two categories list and add on menu.
Using add one can add records in any of three entities.
Using list one can retrieve records stored in three.
When listing clickable records appear ,clicking on these one can get detailed view of a record.

### How do I get set up? ###

* Simply do a git clone of code repo.
install node_modules.
run node app.js
and listen it on localhost:3000.

* using mongodb at localhost:27017.
* mongoose,express 4,passport,express-session.
* Database configuration
host:localhost port:27017.
* Deployment instructions
currently simply cloning git and then git pull and restarting pm2 using 'pm2 restart all' later a automated deployment script using gruntjs can be used.


### Who do I talk to? ###
Sonam yadav
* sonalika.9090@gmail.com