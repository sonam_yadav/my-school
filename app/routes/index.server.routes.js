/**
 * Created by comtech on 16/4/16.
 */
var index = require('../../app/controllers/index.server.controller');

module.exports = function(app) {
    console.log('here in route index')
    app.route('/').get(index.render);
};